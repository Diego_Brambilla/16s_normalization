#!/usr/bin/python

"""
Python script for the normalization of ARG-like sequence
Occurrence respect to the annotated 16S rRNA-like sequences 

Formula:
ABN[ARG] =  ARG-like-reads*(rlength/ARG-gene-length )/
( 16S-reads*rlength/16S-gene-length )


Usage:

$python 16Snormalize.py [arg1] [arg] [rlength] [16S-gene-length] 

arg1: input sorted tabular file with annotation
arg2: reference database: tab-separated file with
feature IDs and feature length
rlength: average length of raw reads, default 150
16S-gene-length: length of the reference 16S marker
gene, default 1432
output-file (file extension must be included)
...


Test run:

$cd 16S_normalization
$python 16Snormalize.py test.argdb.m8 deeparg.features.length.tsv 100 1432 output.normalization.tsv 
"""
import io
import sys
import string
import pandas as pd
#import click


if len(sys.argv) == 0:
	print (__doc__)
	exit(0)


#import sorted short-reads alignment file 
arg=pd.read_csv(sys.argv[1], sep='\t', index_col=0, header=None)

#import
