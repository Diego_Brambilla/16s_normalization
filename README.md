# 16S_normalization

Development of a python code to normalize counts from a read-based annotation matrix to the respective 16S rRNA sequences.

Freely inspired from [DeepARG normalization script](https://bitbucket.org/gusphdproj/deeparg-ss/src/master/deeparg/abn.py)
